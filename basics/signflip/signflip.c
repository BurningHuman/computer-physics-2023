#include <stdio.h>
#include <stdlib.h>

#define LENGTH 30

int main()
{
	float data[LENGTH] = {5623,414,25,-6,-1234,346,0,2146,324,-25643,-534,16,-0,4,6,13,-24,-6,434,5,6,6,7,8,9,34,213,89,56,234};
	float comp1 = 0;
	float comp2 = 0;
	for(unsigned int i = 1; i < LENGTH; i++)
	{
		comp1 = data[i-1];
		comp2 = data[i];
		if((comp1 > 0 && comp2 < 0) || (comp1 < 0 && comp2 > 0) || (comp1 == 0) || (comp2 == 0)){fprintf(stdout, "Found a intersection at between %f and %f!\n", data[i-1], data[i]);}
	}
	return 0;
}

