	.file	"signflip.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC8:
	.string	"Found a intersection at between %f and %f!\n"
	.section	.text.startup,"ax",@progbits
	.p2align 4
	.globl	main
	.type	main, @function
main:
.LFB22:
	.cfi_startproc
	movabsq	$4857695149186088960, %rax
	pushq	%r12
	.cfi_def_cfa_offset 16
	.cfi_offset 12, -16
	pxor	%xmm2, %xmm2
	xorl	%r12d, %r12d
	pushq	%rbp
	.cfi_def_cfa_offset 24
	.cfi_offset 6, -24
	pushq	%rbx
	.cfi_def_cfa_offset 32
	.cfi_offset 3, -32
	addq	$-128, %rsp
	.cfi_def_cfa_offset 160
	movaps	.LC0(%rip), %xmm0
	movq	%rax, 112(%rsp)
	movq	%rsp, %rbx
	leaq	116(%rsp), %rbp
	movaps	%xmm0, (%rsp)
	movaps	.LC1(%rip), %xmm0
	movaps	%xmm0, 16(%rsp)
	movaps	.LC2(%rip), %xmm0
	movaps	%xmm0, 32(%rsp)
	movaps	.LC3(%rip), %xmm0
	movaps	%xmm0, 48(%rsp)
	movaps	.LC4(%rip), %xmm0
	movaps	%xmm0, 64(%rsp)
	movaps	.LC5(%rip), %xmm0
	movaps	%xmm0, 80(%rsp)
	movaps	.LC6(%rip), %xmm0
	movaps	%xmm0, 96(%rsp)
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L5:
	ucomiss	%xmm2, %xmm0
	setnp	%al
	cmovne	%r12d, %eax
	testb	%al, %al
	jne	.L4
	ucomiss	%xmm2, %xmm1
	setnp	%dl
	cmove	%edx, %eax
	testb	%al, %al
	jne	.L4
	addq	$4, %rbx
	cmpq	%rbx, %rbp
	je	.L18
.L9:
	movss	(%rbx), %xmm0
	movss	4(%rbx), %xmm1
	comiss	%xmm2, %xmm0
	jbe	.L2
	comiss	%xmm1, %xmm2
	ja	.L4
.L2:
	comiss	%xmm0, %xmm2
	jbe	.L5
	comiss	%xmm2, %xmm1
	jbe	.L5
.L4:
	movq	stdout(%rip), %rdi
	addq	$4, %rbx
	cvtss2sd	%xmm0, %xmm0
	cvtss2sd	%xmm1, %xmm1
	leaq	.LC8(%rip), %rsi
	movl	$2, %eax
	call	fprintf@PLT
	pxor	%xmm2, %xmm2
	cmpq	%rbx, %rbp
	jne	.L9
.L18:
	subq	$-128, %rsp
	.cfi_def_cfa_offset 32
	xorl	%eax, %eax
	popq	%rbx
	.cfi_def_cfa_offset 24
	popq	%rbp
	.cfi_def_cfa_offset 16
	popq	%r12
	.cfi_def_cfa_offset 8
	ret
	.cfi_endproc
.LFE22:
	.size	main, .-main
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	1169143808
	.long	1137639424
	.long	1103626240
	.long	-1061158912
	.align 16
.LC1:
	.long	-996524032
	.long	1135411200
	.long	0
	.long	1158029312
	.align 16
.LC2:
	.long	1134690304
	.long	-959949312
	.long	-1006272512
	.long	1098907648
	.align 16
.LC3:
	.long	0
	.long	1082130432
	.long	1086324736
	.long	1095761920
	.align 16
.LC4:
	.long	-1044381696
	.long	-1061158912
	.long	1138294784
	.long	1084227584
	.align 16
.LC5:
	.long	1086324736
	.long	1086324736
	.long	1088421888
	.long	1090519040
	.align 16
.LC6:
	.long	1091567616
	.long	1107820544
	.long	1129644032
	.long	1118961664
	.ident	"GCC: (Debian 10.2.1-6) 10.2.1 20210110"
	.section	.note.GNU-stack,"",@progbits
