#include <stdio.h>
#include <math.h>


#define E_CHARGE 1.60217e-19
#define E_0 8.85418e-12
#define PI 3.141592
#define R_0 0.564e-9

double madelung_3d(double precision)
{
	double cube, madelung, last_madelung, energy, distance, n2, j2, i2;
	int charge, symmetry_factor, charge_factor;

	cube = 0;
	madelung = 0;
	last_madelung = 1;

	for(int n = 1; fabs(2*(madelung-last_madelung)/(madelung+last_madelung)) > precision; n++)
	{
		n2 = n*n;
		last_madelung = madelung;

		madelung = cube;

		//loop over j axis
		for(int j = 0; j < n+1; j++)
		{
			j2 = j*j;
			for(int i = j; i < n+1; i++)
			{
				i2 = i*i;
				
				if(j == 0)
				{
					symmetry_factor = (i == 0)*6 + ((i < n)*(i != 0))*24 + (i == n)*12;
				}
				else if(j < n)
				{
					symmetry_factor = (i == j)*24 + ((i < n)*(i != j))*48 + (i == n)*24;
				}
				else
				{
					symmetry_factor = 8;
				}
			

				//factor account to charge neutrality
				if(j < n)
				{
					charge_factor = (i < n)*2 + (i == n)*4;
				}
				else if((j == n) && (i == n))
				{
					charge_factor = 8;
				}

				charge = (n+i+j)%2? 1 : -1;
				distance = sqrt(n2+i2+j2);

				energy = charge * symmetry_factor / distance;

				cube += energy;
				madelung += energy / charge_factor;
				
				printf("Index: [%d\t%d\t%d]\tmadelung: %f\tcube: %f\tsymmetry_factor: %d\n", n, j, i, madelung, cube, symmetry_factor);
			}
		}
	}
	return madelung;
}
double madelung_2d(double precision)
{
	double cube, madelung, last_madelung, energy, distance, n2, j2;
	int charge, symmetry_factor, charge_factor;

	cube = 0;
	madelung = 0;
	last_madelung = 1;

	for(int n = 1; fabs(2*(madelung-last_madelung)/(madelung+last_madelung)) > precision; n++)
	{
		n2 = n*n;
		last_madelung = madelung;

		madelung = cube;

		//loop over j axis
		for(int j = 0; j < n+1; j++)
		{
			j2 = j*j;
				
			symmetry_factor = ( ( (j == 0)+(j==n) ) > 0)*4 + ( ( (j == 0)+(j==n) )== 0)*8;
			//factor account to charge neutrality
			charge_factor = (j == n)*4 + (j != n)*2;

			charge = (n+j)%2? 1 : -1;
			distance = sqrt(n2+j2);

			energy = charge * symmetry_factor / distance;

			cube += energy;
			madelung += energy / charge_factor;
			
			printf("Index: [%d\t%d]\tmadelung: %f\tcube: %f\tsymmetry_factor: %d\n", n, j, madelung, cube, symmetry_factor);
		}
	}
	return madelung;
}



int main()
{
	double madelung3d, madelung2d, energy3d, energy2d;
	madelung3d = madelung_3d(1e-9);
	printf("Madelung 3D finished!\n\n");
	madelung2d = madelung_2d(1e-9);
	energy3d = madelung3d*E_CHARGE*E_CHARGE*(1/(4*PI*E_0*R_0));
	energy2d = madelung2d*E_CHARGE*E_CHARGE*(1/(4*PI*E_0*R_0));
	
	printf("Final result: %.9lf J\n", madelung3d);
	printf("Final result: %.9lf J\n", madelung2d);
	printf("Final result: %.9lf eV\n", energy3d/E_CHARGE);
	printf("Final result: %.9lf eV\n", energy2d/E_CHARGE);
	return 0;
}
